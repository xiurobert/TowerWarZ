# So, you want to donate
We appreciate it, but we cannot accept donations as of now. However, if you would like to sponsor us, we will not stop
you from doing so.

If you want to sponsor us, please choose a category from below:

1. VPS/Dedicated Server (CI server[Jenkins], GitLab, Sonatype Nexus, JIRA/BugZilla, JavaDocs, Forums, Website)
2. Domain name (Not needed, but if you want, it's fine)