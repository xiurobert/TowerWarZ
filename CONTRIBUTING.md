# Contributions

We appreciate contributions of any kind, however to keep this clean, this is what we want:

1. No other languages except
  - Java 
  - Objective-C (iOS app)
2. Do not delete files without putting a text file with the name of the file you deleted and the reason you deleted it.
3. We need people to do the graphics! We have a reason for making it Open-source! Please, we need contributers! Thanks!
4. Use an IDE. We don't like nasty build failures, this hampers our debugging.

  Supported IDEs:
    -> IntelliJ IDEA 14.1
    -> IntelliJ IDEA 14.0
    -> NetBeans 8
  
  Eclipse is no longer supported

## How to contribute
1. Fork this repo
2. Do your contributions in the fork
3. Make a pull request

Thank you!

-The Lightning Software Foundation

### Issues
We will be using the GitHub Issue tracker until we get BugZilla up

1. Title: [HELP/BUG/FEATURE REQUEST]<->(Issue)
2. Body: (Issue) {newline} (Logs)
3. Duplicates will be REJECTED instantly
4. Proprietary usage -> See the GPL
